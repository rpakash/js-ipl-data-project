const fs = require("fs");
const csvParse = require("csv-parser");
const path = require("path");

function matchesWonPerTeamPerYear() {
  const matchesWon = {};

  fs.createReadStream(path.join(__dirname, "../data/matches.csv"))
    .pipe(csvParse())
    .on("data", (match) => {
      if (match.result === "no result") {
        return;
      }

      matchesWon[match.season]
        ? matchesWon[match.season][match.winner]
          ? (matchesWon[match.season][match.winner] += 1)
          : (matchesWon[match.season][match.winner] = 1)
        : (matchesWon[match.season] = { [match.winner]: 1 });
    })
    .on("end", () => {
      fs.writeFileSync(
        path.join(
          __dirname,
          "../public/output/2-matches-won-per-team-per-year.json"
        ),
        JSON.stringify(matchesWon)
      );
    });
}

matchesWonPerTeamPerYear();

module.exports = matchesWonPerTeamPerYear;
