const fs = require("fs");
const csvParse = require("csv-parser");
const path = require("path");

function extraRunsConcededPerTeamInYear(year) {
  const extraRunsPerTeam = {};
  let matchStartId;
  let matchStopId;

  fs.createReadStream(path.join(__dirname, "../data/matches.csv"))
    .pipe(csvParse())
    .on("data", (match) => {
      if (year === +match.season) {
        if (!matchStartId) {
          matchStartId = match.id;
        } else {
          matchStopId = match.id;
        }
      }
    })
    .on("end", () => {
      fs.createReadStream(path.join(__dirname, "../data/deliveries.csv"))
        .pipe(csvParse())
        .on("data", (delivery) => {
          if (
            +delivery.match_id >= matchStartId &&
            +delivery.match_id <= matchStopId
          ) {
            extraRunsPerTeam[delivery.bowling_team]
              ? (extraRunsPerTeam[delivery.bowling_team] +=
                  +delivery.extra_runs)
              : (extraRunsPerTeam[delivery.bowling_team] =
                  +delivery.extra_runs);
          }
        })
        .on("end", () => {
          fs.writeFileSync(
            path.join(
              __dirname,
              "../public/output/3-extra-runs-conceded-per-team.json"
            ),
            JSON.stringify(extraRunsPerTeam)
          );
        });
    });
}

extraRunsConcededPerTeamInYear(2016);

module.exports = extraRunsConcededPerTeamInYear;
