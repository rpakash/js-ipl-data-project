const fs = require("fs");
const csvParse = require("csv-parser");
const path = require("path");

function teamsWonTossAndMatch() {
  let teams = {};

  fs.createReadStream(path.join(__dirname, "../data/matches.csv"))
    .pipe(csvParse())
    .on("data", (match) => {
      if (match.toss_winner === match.winner) {
        teams[match.winner]
          ? (teams[match.winner] += 1)
          : (teams[match.winner] = 1);
      }
    })
    .on("end", () => {
      fs.writeFileSync(
        path.join(
          __dirname,
          "../public/output/5-number-of-times-each-team-has-won-the-toss-and-also-the-match.json"
        ),
        JSON.stringify(teams)
      );
    });
}

teamsWonTossAndMatch();

module.exports = teamsWonTossAndMatch;
