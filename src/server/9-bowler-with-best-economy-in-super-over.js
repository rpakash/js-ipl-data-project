const fs = require("fs");
const csvParse = require("csv-parser");
const path = require("path");

function bowlerWithBestEconomyInSuperOver() {
  let bowlers = {};

  fs.createReadStream(path.join(__dirname, "../data/deliveries.csv"))
    .pipe(csvParse())
    .on("data", (deliver) => {
      if (+deliver.is_super_over) {
        if (bowlers[deliver.bowler] === undefined) {
          bowlers[deliver.bowler] = { runs: 0, balls: 0 };
        }

        if (+deliver.wide_runs || +deliver.noball_runs) {
          bowlers[deliver.bowler].runs +=
            +deliver.total_runs - deliver.penalty_runs;
        } else {
          bowlers[deliver.bowler].runs +=
            +deliver.total_runs -
            (+deliver.bye_runs + +deliver.legbye_runs + +deliver.penalty_runs);
          bowlers[deliver.bowler].balls += 1;
        }
      }
    })
    .on("end", () => {
      const bArrays = Object.entries(bowlers).map((player) => {
        player[1].economy = +(player[1].runs / (player[1].balls / 6)).toFixed(
          2
        );
        player[1].name = player[0];

        return player[1];
      });

      bArrays.sort((a, b) => a.economy - b.economy);

      const economy = bArrays[0].economy;

      const finalBowlers = bArrays.reduce((acc, player) => {
        if (player.economy == economy) {
          let { name, runs, balls, economy } = player;
          acc[player.name] = { runs, balls, economy };

          return acc;
        }

        return acc;
      }, {});

      fs.writeFileSync(
        path.join(
          __dirname,
          "../public/output/9-bowler-with-best-economy-in-super-over.json"
        ),
        JSON.stringify(finalBowlers)
      );
    });
}

bowlerWithBestEconomyInSuperOver();

module.exports = bowlerWithBestEconomyInSuperOver;
