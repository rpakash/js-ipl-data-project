const fs = require("fs");
const csvParse = require("csv-parser");
const path = require("path");

function numberOfTimesPlayerIsDismisedByAnotherPlayer() {
  let playerDismissed = {};

  fs.createReadStream(path.join(__dirname, "../data/deliveries.csv"))
    .pipe(csvParse())
    .on("data", (delivery) => {
      if (delivery.player_dismissed) {
        if (
          !playerDismissed[delivery.player_dismissed + "/" + delivery.bowler]
        ) {
          playerDismissed[
            delivery.player_dismissed + "/" + delivery.bowler
          ] = 1;
        } else {
          playerDismissed[
            delivery.player_dismissed + "/" + delivery.bowler
          ] += 1;
        }
      }
    })
    .on("end", () => {
      let playerArray = Object.entries(playerDismissed).sort(
        (a, b) => b[1] - a[1]
      );
      let dismissedTimes = playerArray[0][1];

      let final = playerArray.reduce((accu, player) => {
        if (player[1] == dismissedTimes) {
          let nameArray = player[0].split("/");

          accu.push({
            batsman: nameArray[0],
            bowler: nameArray[1],
            dismissedTimes: player[1],
          });

          return accu;
        }

        return accu;
      }, []);

      fs.writeFileSync(
        path.join(
          __dirname,
          "../public/output/8-highest-number-of-times-one-player-is-dismissed-by-another-player.json"
        ),
        JSON.stringify(final)
      );
    });
}

numberOfTimesPlayerIsDismisedByAnotherPlayer();

module.exports = numberOfTimesPlayerIsDismisedByAnotherPlayer;
