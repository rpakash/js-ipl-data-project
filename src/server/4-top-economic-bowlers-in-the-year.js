const fs = require("fs");
const csvParse = require("csv-parser");
const path = require("path");

function topEconomicBowlersInTheYear(year, numberOfList) {
  let bowlers = {};
  let matchStartId;
  let matchStopId;

  fs.createReadStream(path.join(__dirname, "../data/matches.csv"))
    .pipe(csvParse())
    .on("data", (row) => {
      if (year == +row.season) {
        if (!matchStartId) {
          matchStartId = +row.id;
        } else {
          matchStopId = +row.id;
        }
      }
    })
    .on("end", () => {
      fs.createReadStream(path.join(__dirname, "../data/deliveries.csv"))
        .pipe(csvParse())
        .on("data", (deliver) => {
          if (
            +deliver.match_id >= matchStartId &&
            +deliver.match_id <= matchStopId
          ) {
            if (bowlers[deliver.bowler] === undefined) {
              bowlers[deliver.bowler] = { runs: 0, balls: 0 };
            }

            if (+deliver.wide_runs || +deliver.noball_runs) {
              bowlers[deliver.bowler].runs +=
                +deliver.total_runs - deliver.penalty_runs;
            } else {
              bowlers[deliver.bowler].runs +=
                +deliver.total_runs -
                (+deliver.bye_runs +
                  +deliver.legbye_runs +
                  +deliver.penalty_runs);
              bowlers[deliver.bowler].balls += 1;
            }
          }
        })
        .on("end", () => {
          let topBowlers = {};
          let bArrays = [];

          Object.keys(bowlers).forEach((bowler) => {
            bowlers[bowler].economy = +(
              bowlers[bowler].runs /
              (bowlers[bowler].balls / 6)
            ).toFixed(2);
            bowlers[bowler].name = bowler;
            bArrays.push(bowlers[bowler]);
          });

          bArrays.sort((a, b) => a.economy - b.economy);

          numberOfList === undefined ? (numberOfList = 10) : numberOfList;

          bArrays.slice(0, numberOfList).forEach((bowler) => {
            let name = bowler.name;
            delete bowler.name;
            topBowlers[name] = bowler;
          });

          fs.writeFileSync(
            path.join(
              __dirname,
              "../public/output/4-top-economical-bowlers-in-the-year.json"
            ),
            JSON.stringify(topBowlers)
          );
        });
    });
}

topEconomicBowlersInTheYear(2015, 10);

module.exports = topEconomicBowlersInTheYear;
