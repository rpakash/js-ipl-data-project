const csvParse = require("csv-parser");
const fs = require("fs");
path = require("path");

function matchesPerYear() {
  let yearsWithMatch = {};

  fs.createReadStream(path.join(__dirname, "../data/matches.csv"))
    .pipe(csvParse())
    .on("data", (match) => {
      let year = match.season;
      yearsWithMatch[year]
        ? (yearsWithMatch[year] += 1)
        : (yearsWithMatch[year] = 1);
    })
    .on("end", () => {
      fs.writeFileSync(
        path.join(__dirname, "../public/output/1-matches-per-year.json"),
        JSON.stringify(yearsWithMatch)
      );
    });
}

matchesPerYear();

module.exports = matchesPerYear;
