const fs = require("fs");
const path = require("path");
const csvParser = require("csv-parser");

function strikeRateOfBatsman(name) {
  let player = {};
  let matchYear = {};

  fs.createReadStream(path.join(__dirname, "../data/matches.csv"))
    .pipe(csvParser())
    .on("data", (match) => {
      matchYear[match.id] = match.season;
    })
    .on("end", () => {
      fs.createReadStream(path.join(__dirname, "../data/deliveries.csv"))
        .pipe(csvParser())
        .on("data", (delivery) => {
          if (delivery.batsman === name) {
            if (player[matchYear[delivery.match_id]] === undefined) {
              player[matchYear[delivery.match_id]] = {
                runs: 0,
                balls: 0,
              };
            }

            if (+delivery.wide_runs !== 0) {
              player[matchYear[delivery.match_id]].runs +=
                +delivery.batsman_runs;
            } else {
              player[matchYear[delivery.match_id]].runs +=
                +delivery.batsman_runs;
              player[matchYear[delivery.match_id]].balls += 1;
            }
          }
        })
        .on("end", () => {
          Object.keys(player).forEach((year) => {
            player[year].strikeRate = (
              (player[year].runs * 100) /
              player[year].balls
            ).toFixed(2);
          });

          fs.writeFileSync(
            path.join(
              __dirname,
              "../public/output/7-strike-rate-of-a-batsman-for-each-season.json"
            ),
            JSON.stringify(player)
          );
        });
    });
}

strikeRateOfBatsman("PA Patel");

module.exports = strikeRateOfBatsman;
