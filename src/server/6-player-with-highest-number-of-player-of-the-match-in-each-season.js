const fs = require("fs");
const path = require("path");
const csvParser = require("csv-parser");

function highestPlayerOfTheMatchTitle() {
  let playersWithTitle = {};

  fs.createReadStream(path.join(__dirname, "../data/matches.csv"))
    .pipe(csvParser())
    .on("data", (match) => {
      if (match.result === "no result") {
        return;
      }

      if (playersWithTitle[match.season] == undefined) {
        playersWithTitle[match.season] = {};
      }

      if (playersWithTitle[match.season][match.player_of_match] === undefined) {
        playersWithTitle[match.season][match.player_of_match] = 1;
      } else {
        playersWithTitle[match.season][match.player_of_match] += 1;
      }
    })
    .on("end", () => {
      Object.keys(playersWithTitle).forEach((year) => {
        let players = [];
        let numberOfTitle = 0;

        Object.keys(playersWithTitle[year]).forEach((player) => {
          if (playersWithTitle[year][player] > numberOfTitle) {
            players = [player];
            numberOfTitle = playersWithTitle[year][player];
          } else if (playersWithTitle[year][player] == numberOfTitle) {
            players.push(player);
          }
        });

        playersWithTitle[year] = {};

        players.forEach((player) => {
          playersWithTitle[year][player] = numberOfTitle;
        });
      });

      fs.writeFileSync(
        path.join(
          __dirname,
          "../public/output/6-player-with-highest-number-of-player-of-the-match-in-each-season.json"
        ),
        JSON.stringify(playersWithTitle)
      );
    });
}

highestPlayerOfTheMatchTitle();

module.exports = highestPlayerOfTheMatchTitle;
